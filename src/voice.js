let obj = {};

const url = 'https://api.cognitive.microsoft.com/sts/v1.0';
const urlSynth = 'https://speech.platform.bing.com/'
const key = '5ba549a54c064ba280d4ee724e3f2166';

let token;

let getToken = async () => {
  const request = new Request(
    `${url}/issueToken`, {  
      method: 'post',  
      headers: {  
        'Ocp-Apim-Subscription-Key': key  
      }
    }
  );
  try {
    let response = await fetch(request);
    return response.text();
  } catch (error) {
    console.error('Request failed', error);
    throw error;
  }
};

obj.init = () => {
  return getToken().then(data => { 
    token = data;
    return data;
  });
};

obj.getSpeechUrl = async (text) => {  
  if (!token) await obj.init();
  // token = token || await getToken();

  const request = new Request(
    `${urlSynth}/synthesize`, {  
      method: 'post',  
      headers: {  
        'Content-Type' : 'application/ssml+xml',
        'X-Microsoft-OutputFormat' : 'riff-16khz-16bit-mono-pcm', 
        'Authorization': 'Bearer ' + token
      },
      body: `<speak version="1.0" xmlns="http://www.w3.org/2001/10/synthesis" xmlns:mstts="http://www.w3.org/2001/mstts" xml:lang="de-DE"><voice xml:lang="de-DE" name="Microsoft Server Speech Text to Speech Voice (de-DE, Stefan, Apollo)">${text}</voice></speak>`
    }
  );

  try {
    let response = await fetch(request);
    let soundBlob = await response.blob();
    // URL.revokeObjectURL() to clean up
    return URL.createObjectURL(soundBlob);
  } catch (error) {
    console.error('Request failed', error);
    throw error;
  }

};


export default obj;

  