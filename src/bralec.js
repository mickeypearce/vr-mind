let obj = {};

const urlSynth = 'http://ebralec.si/branje/preberi.asp';

// Api checks origin or host to match
// So we have to use proxy that correctly sets them
const proxify = url => {
  const urlProxy = 'http://www.kredarca.si/proxy/';
  return `${urlProxy}${url}`;
};

obj.getSpeechUrl = async text => {
  const options = {
    besedilo: text,
    glas: 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\eBraMaja',
    hitrost: 0,
    sirina: 519
  };
  const params = new URLSearchParams(Object.entries(options));
  // Req params
  /*var params = new URLSearchParams(); 
  params.append('besedilo', 'To je besedilo moje je tako');
  params.append('hitrost', 0);
  params.append('glas', 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\eBraMaja');
  params.append('sirina', 519);
  */

  const request = new Request(proxify(urlSynth), {
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    },
    body: params.toString()

    // ,mode: 'no-cors'
  });

  try {
    let response = await fetch(request);
    // Response text contains audio element with speech
    // EX.
    // <p class="poravnanoSredina"><audio controls autoplay>
    // <source src="/_private/Rend/27800475-0.mp3" type="audio/mpeg">
    // <source src="/_private/Rend/27800475-0.ogg" type="audio/ogg">
    // </audio></p>
    let text = await response.text();
    // Parse response
    let parser = new DOMParser();
    let doc = parser.parseFromString(text, 'text/html');
    let source = doc.querySelectorAll('[type="audio/mpeg"]')[0];
    let url = source.getAttribute('src');
    // Src is relative to origin
    let origin = new URL(urlSynth).origin;
    return proxify(origin + url);
  } catch (error) {
    console.error('Request failed', error);
    throw error;
  }
};

export default obj;
