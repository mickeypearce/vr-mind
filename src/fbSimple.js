let obj = {};

const BASE_URL = 'https://geo.example.org/api';
const TOKEN_NAME = 'vrmind:fbtoken';

function buildUrlWithParams({ base = BASE_URL, path = '/', params = {} }) {
  let url = new URL(path, base);
  // url.searchParams = new URLSearchParams(Object.entries(params));
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
  return url;
}

/*URL.prototype.addSearchParams = function(params) {
  Object.keys(params).forEach(key => this.searchParams.append(key, params[key]));
};*/

const addSearchParams = (url, params) => {
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
};

function setToken(token) {
  return localStorage.setItem(TOKEN_NAME, token);
}

function getToken() {
  return localStorage.getItem(TOKEN_NAME);
}

obj.init = () => {
  const options = { a: 'foo', b: 'bar' };

  const url = new URL('init', BASE_URL);
  addSearchParams(url, options);

  /*let url = new URL(path, BASE_URL);
  const urlParams = new URLSearchParams(Object.entries(params));
  url.searchParams = urlParams;
  */

  // const url = buildUrlWithParams({
  //   path: 'init',
  //   params: { a: 'foo', b: 'bar' }
  // });

  // url.addSearchParams(options);

  const request = new Request(url, {
    method: 'post'
  });
  return fetch(request);
};

obj.getFeed = () => {
  const request = new Request(`${url}/me/feed`, {
    method: 'post',
    params: {
      access_token: getToken()
    }
  });
  return fetch(request);
};

export default obj;
