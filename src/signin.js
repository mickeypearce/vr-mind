import * as firebase from 'firebase';
// import loadFBSDK from 'facebook-web-sdk-promise';
import facebook from './fb';

let fb;

const config = {
  apiKey: "AIzaSyCKWeBjoiKD2I6XrJcP3xw7joPyzPK7o78",
  authDomain: "vr-mind.firebaseapp.com",
  databaseURL: "https://vr-mind.firebaseio.com",
  projectId: "vr-mind",
  storageBucket: "vr-mind.appspot.com",
  messagingSenderId: "260226398352"
};

function initialize() {  
  firebase.initializeApp(config);  
};

function toggleSignIn() {
  if (!firebase.auth().currentUser) {    
    var provider = new firebase.auth.FacebookAuthProvider();    
    provider.addScope('user_posts');    

    firebase.auth().signInWithPopup(provider).then(function(result) {      
      var token = result.credential.accessToken;
      // window.localStorage.setItem('vrmind:token', token);
      fb.setToken(token);

      // The signed-in user info.
      // var user = result.user;
    }).catch((error) => console.error(error));
  } else {
    firebase.auth().signOut();
  }
};

function initApp() {

  firebase.auth().onAuthStateChanged(async (user) => {
    if (user) {
      
      fb = await facebook();
      
      if (await fb.isConnected()) {
        try {
          let feed = await fb.getFeed();
          console.log('fb feed='+feed.data);
        } catch (error) {
          console.log(error.message);
        }
      };
      

      document.getElementById('quickstart-sign-in').textContent = `Log out, ${user.displayName}`;      
    } else {      
      document.getElementById('quickstart-sign-in').textContent = 'Log in with Facebook';      
    }    
    document.getElementById('quickstart-sign-in').disabled = false;    
  });  
  document.getElementById('quickstart-sign-in').addEventListener('click', toggleSignIn, false);
};

export {initialize, toggleSignIn, initApp};