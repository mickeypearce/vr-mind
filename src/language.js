let obj = {};

const BASE_URL =
  'https://westus.api.cognitive.microsoft.com/text/analytics/v2.0';
const key = '5ba549a54c064ba280d4ee724e3f2166';

obj.getDetectLanguage = async text => {
  let url = new URL('languages', BASE_URL);
  let body = {
    documents: [
      {
        id: 'string1',
        text
      }
    ]
  };

  const request = new Request(url, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      'Ocp-Apim-Subscription-Key': key
    },
    body: body.toString()
  });

  try {
    let response = await fetch(request);
    return response.text();
  } catch (error) {
    console.error('Request failed', error);
    throw error;
  }
};

export default obj;
