import aframe from 'aframe';
import firebase from 'firebase';

import facebook from './fb';
import voice from './voice';
import bralec from './bralec';

// import {initialize, toggleSignIn, initApp} from './signin';

const createSoundEntity = async text => {
  const soundUrl = await voice.getSpeechUrl(text);
  // let soundUrl = await bralec.getSpeechUrl(text);
  // let soundUrl = 'url(resources/download.wav)';

  let entityEl = document.createElement('a-entity');
  entityEl.setAttribute('geometry', {
    primitive: 'box',
    height: 2,
    width: 1
  });
  entityEl.setAttribute('sound', {
    src: soundUrl,
    autoplay: true
    //,
    // on: 'click'
  });

  return entityEl;
};

const loadSounds = async () => {
  let sceneEl = document.querySelector('a-scene');

  const fb = await facebook.initSDK();

  if (await fb.isConnected()) {
    const respFeed = await fb.getFeed();

    const feed = respFeed.data.slice(1, 3);

    feed.forEach(async post => {
      let soundEl = await createSoundEntity(post.message);
      sceneEl.appendChild(soundEl);

      console.log(`Sound loaded: ${post.message}.`);
    });
  }
};

const config = {
  apiKey: 'AIzaSyCKWeBjoiKD2I6XrJcP3xw7joPyzPK7o78',
  authDomain: 'vr-mind.firebaseapp.com',
  databaseURL: 'https://vr-mind.firebaseio.com',
  projectId: 'vr-mind',
  storageBucket: 'vr-mind.appspot.com',
  messagingSenderId: '260226398352'
};

function toggleSignIn() {
  if (!firebase.auth().currentUser) {
    var provider = new firebase.auth.FacebookAuthProvider();
    provider.addScope('user_posts');

    firebase
      .auth()
      .signInWithPopup(provider)
      .then(function(result) {
        var token = result.credential.accessToken;
        // window.localStorage.setItem('vrmind:token', token);
        facebook.setToken(token);

        // The signed-in user info.

        // var user = result.user;
      })
      .catch(error => console.error(error));
  } else {
    firebase.auth().signOut();
  }
}

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(user => {
  if (user) {
    document.getElementById(
      'quickstart-sign-in'
    ).textContent = `Log out, ${user.displayName}`;
    loadSounds();

    //Play sounds

    // var sounds = document.querySelectorAll('[sound]');

    // sounds.forEach(sound => {

    //   components.sound.playSound()

    // });
  } else {
    document.getElementById('quickstart-sign-in').textContent =
      'Log in with Facebook';
  }
});

document
  .getElementById('quickstart-sign-in')
  .addEventListener('click', toggleSignIn, false);
