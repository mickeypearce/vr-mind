import loadFBSDK from 'facebook-web-sdk-promise';

let _token;

const setToken = token => {
  _token = token;
  localStorage.setItem('vrmind:fbtoken', token);
};

const api = async () => {
  // FB api object
  // let FB;
  // Token is stored in localstorage
  let _token;

  // const init = async () => {
  // Load FB SDK
  const FB = await loadFBSDK();
  FB.init({
    appId: '141206669235256',
    autoLogAppEvents: true,
    // status: true,
    xfbml: false,
    version: 'v2.9'
  });
  _token = localStorage.getItem('vrmind:fbtoken');
  console.log('Facebook SDK loaded and initialized!');
  // };

  const isConnected = () =>
    new Promise((res, rej) => {
      FB.getLoginStatus(resp => {
        if (resp.status === 'connected') res(true);
        rej(false);
      });
    });
  const getFeed = () =>
    new Promise((res, rej) => {
      FB.api('/me/feed', { access_token: _token }, resp => {
        if (!resp || resp.error) rej(resp.error);
        res(resp);
      });
    });

  // init();
  return {
    isConnected,
    getFeed
  };
};

export default { initSDK: api, setToken: setToken };
